class Contact < ActiveRecord::Base
  validates :first_name, :last_name, :number, presence: true
  validates :number, format: { with: /\A(\d+)\z/ }
  validates :number, length: { in: 10..13 }

  scope :search, ->(search_regexp, search_digits){ where('first_name REGEXP ? or last_name REGEXP ? or number like ?', search_regexp, search_regexp, "%#{search_digits}%") }

  def self.prepare_search_regexp(query_params)
    search_regexp = ''
    query_params.chars.each do |digit|
      search_regexp << regexp_group_for(digit)
    end
    search_regexp
  end

  def self.regexp_group_for(digit)
    PHONE_KEYPAD_ALPHABETS[:"#{digit}"]
  end

  def self.perform_search(query_params)
    search_regexp = prepare_search_regexp(query_params)
    self.search(search_regexp, query_params)
  end

  def display_full_name
    "#{first_name} #{last_name}"
  end
end
