json.array!(@contacts) do |contact|
  json.extract! contact, :id, :first_name, :last_name, :number, :display_full_name
  json.url contact_url(contact)
end
