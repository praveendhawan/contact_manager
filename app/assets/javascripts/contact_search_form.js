function ContactSearchForm(elements) {
  this.$input_field = elements.input_field;
  this.$input_buttons_container = elements.input_buttons_container;
  this.$dummy_contacts_container = elements.dummy_contacts_container;
}

ContactSearchForm.prototype.init = function () {
  this.bindEvents();
};

ContactSearchForm.prototype.bindEvents = function () {
  var _this = this;
  this.$input_buttons_container.on('click', '[data-class="input-buttons"]', _this.prepareAndSendAjax());
};


ContactSearchForm.prototype.prepareAndSendAjax = function () {
  var _this = this;
  return function(){
    var $this = $(this);
    _this.copyTextToInputField($this);
    options = _this.prepareAjaxOptions();
    _this.sendAjax(options);
  };
};

ContactSearchForm.prototype.copyTextToInputField = function (clicked_button) {
  prev_value = this.$input_field.val();
  value = prev_value + clicked_button.find('span.digit').data("value");
  this.$input_field.val(value);
};

ContactSearchForm.prototype.prepareAjaxOptions = function (options) {
  return {
    url: this.$input_field.data('url'),
    data: { q: { search: this.$input_field.val() } },
    method: 'get',
    dataType: 'json'
  };
};

ContactSearchForm.prototype.sendAjax = function () {
  var _this = this;
  $.ajax({
    url: options.url,
    dataType: options.dataType,
    data: options.data,
    method: options.method
  }).success(function(data){
    _this.handleAjaxSuccess(data);
  }).error(function(xhr){
    _this.handleAjaxFailure(xhr);
  });
};

ContactSearchForm.prototype.handleAjaxSuccess = function (data) {
  var _this = this,
  contacts_container = $('[data-class="contacts-container"]');
  contacts_container.empty();
  $(data).each(function(index, element){
    var container = _this.$dummy_contacts_container.clone();
    container.attr('data-id', element.id);
    container.find('[data-class="contact-name"] a').attr('href', element.url).text(element.display_full_name);
    container.find('[data-class="contact-number"]').text(element.number);
    contacts_container.append(container.removeAttr('data-class').removeClass('hidden'));
  });
};

ContactSearchForm.prototype.handleAjaxFailure = function (xhr) {
};



$(function(){
  var elements = {
    input_field: $('[data-class="search-input"]'),
    input_buttons_container: $('.digit-buttons-container'),
    dummy_contacts_container: $('[data-class="dummy-contacts-container"] [data-class="dummy-row"]')
  };
  contact_search_form = new ContactSearchForm(elements);
  contact_search_form.init();
});
