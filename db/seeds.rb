# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Contact.create(first_name: 'Alex', last_name: 'Sinatra', number: '9999999999')
Contact.create(first_name: 'Harry', last_name: 'Potter', number: '8888888888')
Contact.create(first_name: 'Angel', last_name: 'Dravid', number: '9889998898')
